﻿using System.Net.Mail;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Rnwood.SmtpServer.Tests
{
    [TestClass]
    public class ClientTests
    {
        private Server server;

        [TestInitialize]
        public void TestInitialize()
        {
            server = StartServerWithWait();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            server.Stop();
        }

        [TestMethod]
        public void SmtpClient_NonSSL()
        {
            using (var client = new SmtpClient("localhost", server.PortNumber))
            {
                client.Send("from@from.com", "to@to.com", "subject", "body");
            }
        }

        private Server StartServerWithWait()
        {
            Server server = new DefaultServer(Ports.AssignAutomatically);
            server.Start();
            for (int i = 0; i < 10; i++)
            {
                if (server.IsRunning)
                {
                    break;
                }

                Thread.Sleep(1000);
            }

            return server;
        }
    }
}
