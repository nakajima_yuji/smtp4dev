﻿using System;
using System.Collections.Generic;

namespace Rnwood.SmtpServer.ConnectionVerbs.MailSubVerbs
{
    internal class ParameterParser
    {
        private readonly List<Parameter> _parameters = new List<Parameter>();

        public ParameterParser(string[] tokens)
        {
            ParameterText = string.Join(" ", tokens);
            Parse(tokens);
        }

        public ParameterParser(string parameterText)
        {
            ParameterText = parameterText;
            Parse(ParameterText.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries));
        }

        public Parameter[] Parameters
        {
            get { return _parameters.ToArray(); }
        }

        private string ParameterText { get; set; }

        private void Parse(string[] tokens)
        {
            foreach (string token in tokens)
            {
                string[] tokenParts = token.Split(new[] {'='}, 2, StringSplitOptions.RemoveEmptyEntries);
                string key = tokenParts[0];
                string value = tokenParts.Length > 1 ? tokenParts[1] : null;
                _parameters.Add(new Parameter(key, value));
            }
        }
    }
}