﻿using System.Linq;

namespace Rnwood.SmtpServer.Extensions.Auth
{
    internal class AuthVerb : IVerb
    {
        private readonly AuthExtensionProcessor authExtensionProcessor;

        public AuthVerb(AuthExtensionProcessor authExtensionProcessor)
        {
            this.authExtensionProcessor = authExtensionProcessor;
        }

        public void Process(IConnection connection, SmtpCommand command)
        {
            if (command.Arguments.Count == 0) throw new SmtpServerException(new SmtpResponse(StandardSmtpResponseCode.SyntaxErrorInCommandArguments, "Must specify AUTH mechanism as a parameter"));
            if (connection.Authenticated) throw new SmtpServerException(new SmtpResponse(StandardSmtpResponseCode.BadSequenceOfCommands, "Already authenticated"));

            var mechanismId = command.Arguments[0];
            var mechanism = this.authExtensionProcessor.MechanismMap.Get(mechanismId);
            if (mechanism == null) throw new SmtpServerException(new SmtpResponse(StandardSmtpResponseCode.CommandParameterNotImplemented, "Specified AUTH mechanism not supported"));
            if (!this.authExtensionProcessor.IsMechanismEnabled(mechanism)) throw new SmtpServerException(new SmtpResponse(StandardSmtpResponseCode.AuthenticationFailure, "Specified AUTH mechanism not allowed right now (might require secure connection etc)"));

            var authMechanismProcessor = mechanism.CreateAuthMechanismProcessor(connection);
            var result = authMechanismProcessor.ProcessRequest(string.Join(" ", command.Arguments.Skip(1).ToArray()));
            while (result.Status == AuthMechanismProcessorStatus.Continue)
            {
                connection.WriteResponse(result.Response);
                string response = connection.ReadLine();
                if (response == "*")
                {
                    connection.WriteResponse(new SmtpResponse(StandardSmtpResponseCode.SyntaxErrorInCommandArguments, "Authentication aborted"));
                    return;
                }

                result = authMechanismProcessor.ProcessRequest(response);
            }

            if (result.Status == AuthMechanismProcessorStatus.Failed)
            {
                connection.WriteResponse(new SmtpResponse(StandardSmtpResponseCode.AuthenticationFailure, "Authentication failure"));
                return;
            }

            connection.WriteResponse(new SmtpResponse(StandardSmtpResponseCode.AuthenitcationOK, "Authenticated OK"));
        }
    }
}
