﻿using System.Linq;

namespace Rnwood.SmtpServer.ConnectionVerbs.RcptSubVerbs
{
    internal class RcptToVerb : IVerb
    {
        public void Process(IConnection connection, SmtpCommand command)
        {
            if (connection.CurrentMessage == null)
            {
                connection.WriteResponse(new SmtpResponse(StandardSmtpResponseCode.BadSequenceOfCommands, "No current message"));
                return;
            }

            if (command.ArgumentsText == "<>"
                || !command.ArgumentsText.StartsWith("<")
                || !command.ArgumentsText.EndsWith(">")
                || command.ArgumentsText.Count(c => c == '<') != command.ArgumentsText.Count(c => c == '>'))
            {
                connection.WriteResponse(new SmtpResponse(StandardSmtpResponseCode.SyntaxErrorInCommandArguments, "Must specify to address <address>"));
                return;
            }

            var address = command.ArgumentsText.Remove(0, 1).Remove(command.ArgumentsText.Length - 2);
            connection.MessageRecipientAdd(address);
            connection.WriteResponse(new SmtpResponse(StandardSmtpResponseCode.OK, "Recipient accepted"));
        }
    }
}