﻿namespace Rnwood.SmtpServer.Extensions.Auth
{
    public interface IAuthMechanismProcessor
    {
        AuthMechanismProcessorResult ProcessRequest(string data);
    }
}