﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Rnwood.SmtpServer
{
    public class Server
    {
        private readonly IServerBehaviour behaviour;

        private Thread coreThread;
        private TcpListener listener;

        public Server(IServerBehaviour behaviour)
        {
            this.behaviour = behaviour;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the server is currently running.
        /// </summary>
        public bool IsRunning { get; private set; }

        /// <remarks>
        /// for UnitTest
        /// </remarks>
        internal int PortNumber
        {
            get
            {
                return ((IPEndPoint)this.listener.LocalEndpoint).Port;
            }
        }

        /// <summary>
        /// Runs the server synchronously. This method blocks until the server is stopped.
        /// </summary>
        public void Run()
        {
            if (this.IsRunning) throw new InvalidOperationException("Already running");

            this.listener = new TcpListener(this.behaviour.IpAddress, this.behaviour.PortNumber);
            this.listener.Start();

            this.IsRunning = true;

            Core();
        }

        /// <summary>
        /// Runs the server asynchronously. This method returns once the server has been started.
        /// To stop the server call the <see cref="Stop()"/> method.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">if the server is already running.</exception>
        public void Start()
        {
            if (this.IsRunning) throw new InvalidOperationException("Already running");

            this.listener = new TcpListener(this.behaviour.IpAddress, this.behaviour.PortNumber);
            this.listener.Start();

            this.IsRunning = true;

            new Thread(Core).Start();
        }

        /// <summary>
        /// Stops the running server. Any existing connections are continued.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">if the server is not running.</exception>
        public void Stop()
        {
            if (!this.IsRunning) throw new InvalidOperationException("Not running");

            this.IsRunning = false;
            this.listener.Stop();

            this.coreThread.Join();
        }

        private void Core()
        {
            this.coreThread = Thread.CurrentThread;

            try
            {
                while (true)
                {
                    var tcpClient = this.listener.AcceptTcpClient();
                    new Thread(ConnectionThreadWork).Start(tcpClient);
                }
            }
            catch (SocketException e)
            {
                if (e.SocketErrorCode == SocketError.Interrupted)
                {
                    //normal - caused by _listener.Stop();
                }
                else
                {
                    throw;
                }
            }
        }

        private void ConnectionThreadWork(object tcpClient)
        {
            var connection = new Connection(this.behaviour, (TcpClient)tcpClient);
            connection.Start();
        }
    }
}