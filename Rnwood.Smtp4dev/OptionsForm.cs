﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Windows.Forms;
using Rnwood.Smtp4dev.Properties;

namespace Rnwood.Smtp4dev
{
    internal partial class OptionsForm : Form
    {
        public OptionsForm()
        {
            InitializeComponent();

            Icon = Resources.ListeningIcon;
            checkBox3.Checked = RegistrySettings.StartOnLogin;

            UpdateControlStatus();

            ipAddressCombo.DataSource =
                new[] {IPAddress.Any}.Concat(
                    NetworkInterface.GetAllNetworkInterfaces().SelectMany(ni => ni.GetIPProperties().UnicastAddresses).
                        Where(ua => ua.Address.AddressFamily == AddressFamily.InterNetwork).Select(ua => ua.Address)).
                    ToList();
            ipAddressCombo.SelectedItem = IPAddress.Parse(Settings.Default.IPAddress);
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            RegistrySettings.StartOnLogin = checkBox3.Checked;
            Settings.Default.IPAddress = (ipAddressCombo.SelectedItem).ToString();
            Settings.Default.Save();
            DialogResult = DialogResult.OK;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Settings.Default.Reload();
            DialogResult = DialogResult.Cancel;
        }

        private void OpenSSLCertDialogButton_Click(object sender, EventArgs e)
        {
            if (openSSLCertDialog.ShowDialog() == DialogResult.OK)
            {
                Settings.Default.SSLCertificatePath = openSSLCertDialog.FileName;
            }
        }

        private void CheckForUpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Program.CheckForUpdateCore())
                {
                    MessageBox.Show("No update available", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error checking for update.\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AutoViewMessageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateControlStatus();
        }

        private void IntoTrayCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateControlStatus();

            if (!intoTrayCheckBox.Checked)
            {
                intoTrayOnStartupCheckBox.Checked = false;
            }

            if (autoViewMessageCheckBox.Checked)
            {
                BringToFrontCheckBox.Checked = false;
            }
        }

        private void UpdateControlStatus()
        {
            showBaloonCheckBox.Enabled = !autoViewMessageCheckBox.Checked && intoTrayCheckBox.Checked;
            intoTrayOnStartupCheckBox.Enabled = intoTrayCheckBox.Checked;
            BringToFrontCheckBox.Enabled = !autoViewMessageCheckBox.Checked;
        }
    }
}