﻿namespace Rnwood.SmtpServer.ConnectionVerbs.MailSubVerbs
{
    internal class MailSubVerbMap : VerbMap
    {
        public MailSubVerbMap()
        {
            SetVerbProcessor("FROM", new MailFromVerb());
        }
    }
}
