﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Rnwood.SmtpServer.ConnectionVerbs;

namespace Rnwood.SmtpServer.Tests.Verbs
{
    [TestClass]
    public class HeloVerbTests
    {
        [TestMethod]
        public void SayHelo()
        {
            var mocks = new Mocks();

            var verb = new HeloVerb();
            verb.Process(mocks.Connection.Object, new SmtpCommand("HELO foo.blah"));

            mocks.VerifyWriteResponse(StandardSmtpResponseCode.OK);
            mocks.Session.VerifySet(s => s.ClientName, "foo.bar");
        }

        [TestMethod]
        public void SayHeloTwice_ReturnsError()
        {
            var mocks = new Mocks();
            mocks.Session.SetupGet(s => s.ClientName).Returns("already.said.helo");

            var verb = new HeloVerb();
            verb.Process(mocks.Connection.Object, new SmtpCommand("HELO foo.blah"));

            mocks.VerifyWriteResponse(StandardSmtpResponseCode.BadSequenceOfCommands);
            mocks.Session.VerifySet(s => s.ClientName = It.IsAny<string>(), Times.Never());
        }
    }
}
