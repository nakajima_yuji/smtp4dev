rnwoodさんが作成していたテスト用SMTPサーバー。 
すでに活動されていないようなのでホスト元(https://smtp4dev.codeplex.com)のcodeplexがなくなる前にソースコードを確保。（trunkではなくbranchesのsmtp4dev-2.0を使用。） 
ライセンスはNew BSD Licenseとなっていた。 
なお、GitHubに移った模様(https://github.com/rnwood/smtp4dev)だが、こちらはReWriteしているようでものが違う。