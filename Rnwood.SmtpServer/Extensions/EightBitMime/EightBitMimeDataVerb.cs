﻿using System;
using System.Text;
using Rnwood.SmtpServer.ConnectionVerbs;
using Rnwood.SmtpServer.ConnectionVerbs.MailSubVerbs;

namespace Rnwood.SmtpServer.Extensions.EightBitMime
{
    internal class EightBitMimeDataVerb : DataVerb, IParameterProcessor
    {
        private bool _eightBitMessage;

        public void SetParameter(string key, string value)
        {
            if (key.Equals("BODY", StringComparison.InvariantCultureIgnoreCase))
            {
                if (value.Equals("8BITMIME", StringComparison.InvariantCultureIgnoreCase))
                {
                    _eightBitMessage = true;
                }
                else if (value.Equals("7BIT", StringComparison.InvariantCultureIgnoreCase))
                {
                    _eightBitMessage = false;
                }
                else
                {
                    throw new SmtpServerException(new SmtpResponse(StandardSmtpResponseCode.SyntaxErrorInCommandArguments, "BODY parameter value invalid - must be either 7BIT or 8BITMIME"));
                }
            }
        }

        public override void Process(IConnection connection, SmtpCommand command)
        {
            if (_eightBitMessage)
            {
                connection.SetReaderEncoding(Encoding.Default);
            }

            try
            {
                base.Process(connection, command);
            }
            finally
            {
                if (_eightBitMessage)
                {
                    connection.SetReaderEncodingToDefault();
                }
            }
        }
    }
}
