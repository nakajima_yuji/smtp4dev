﻿namespace Rnwood.SmtpServer.Extensions.Auth.Login
{
    internal class LoginAuthenticationRequest : IAuthenticationRequest
    {
        private readonly string username;
        private readonly string password;

        public LoginAuthenticationRequest(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }
}