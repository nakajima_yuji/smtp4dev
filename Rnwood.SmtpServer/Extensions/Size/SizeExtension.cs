﻿namespace Rnwood.SmtpServer.Extensions.Size
{
    public class SizeExtension : IExtension
    {
        private class SizeExtensionProcessor : IExtensionProcessor
        {
            private readonly IConnection connection;

            public SizeExtensionProcessor(IConnection connection)
            {
                this.connection = connection;
                this.connection.MailVerb.FromSubVerb.ParameterProcessorMap.SetProcessor("SIZE", new SizeVerb(connection));
            }

            public string[] EHLOKeywords
            {
                get
                {
                    long? maxMessageSize = this.connection.GetMaximumMessageSize();

                    return maxMessageSize.HasValue ? new[] { $"SIZE={maxMessageSize.Value}" } : new[] { "SIZE" };
                }
            }
        }

        public IExtensionProcessor CreateExtensionProcessor(IConnection connection)
        {
            return new SizeExtensionProcessor(connection);
        }
    }
}