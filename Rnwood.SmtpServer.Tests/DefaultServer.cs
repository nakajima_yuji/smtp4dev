﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Rnwood.SmtpServer.Tests
{
    internal class DefaultServer : Server
    {
        private readonly DefaultServerBehaviour behaviour;

        /// <summary>
        /// Initializes a new SMTP over SSL server on port 465 using the
        /// supplied SSL certificate.
        /// </summary>
        /// <param name="sslCertificate">The SSL certificate to use for the server.</param>
        private DefaultServer(X509Certificate sslCertificate)
            : this(465, sslCertificate)
        {
        }

        /// <summary>
        /// Initializes a new SMTP server on port 25.
        /// </summary>
        private DefaultServer()
            : this(25, null)
        {
        }

        /// <summary>
        /// Initializes a new SMTP server on the specified port number.
        /// </summary>
        /// <param name="portNumber">The port number.</param>
        public DefaultServer(int portNumber)
            : this(portNumber, null)
        {
        }

        /// <summary>
        /// Initializes a new SMTP over SSL server on the specified port number
        /// using the supplied SSL ceritificate.
        /// </summary>
        /// <param name="portNumber">The port number.</param>
        /// <param name="sslCertificate">The SSL certificate.</param>
        private DefaultServer(int portNumber, X509Certificate sslCertificate)
            : this(new DefaultServerBehaviour(portNumber, sslCertificate))
        {
        }

        /// <summary>
        /// Initializes a new SMTP over SSL server on the specified standard port number
        /// </summary>
        /// <param name="portNumber">The port number.</param>
        /// <param name="sslCertificate">The SSL certificate.</param>
        public DefaultServer(Ports port)
            : this(new DefaultServerBehaviour((int)port))
        {
        }

        private DefaultServer(DefaultServerBehaviour behaviour) : base(behaviour)
        {
            this.behaviour = behaviour;
        }

        private event EventHandler<MessageEventArgs> MessageReceived
        {
            add { behaviour.MessageReceived += value; }
            remove { behaviour.MessageReceived -= value; }
        }

        private event EventHandler<SessionEventArgs> SessionCompleted
        {
            add { behaviour.SessionCompleted += value; }
            remove { behaviour.SessionCompleted -= value; }
        }

        private event EventHandler<SessionEventArgs> SessionStarted
        {
            add { behaviour.SessionStarted += value; }
            remove { behaviour.SessionStarted -= value; }
        }

        private event EventHandler<AuthenticationCredentialsValidationEventArgs> AuthenticationCredentialsValidationRequired
        {
            add { behaviour.AuthenticationCredentialsValidationRequired += value; }
            remove { behaviour.AuthenticationCredentialsValidationRequired -= value; }
        }

        private event EventHandler<MessageEventArgs> MessageCompleted
        {
            add { behaviour.MessageCompleted += value; }
            remove { behaviour.MessageCompleted -= value; }
        }
    }
}
