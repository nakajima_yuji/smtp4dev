using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Rnwood.SmtpServer.ConnectionVerbs;
using Rnwood.SmtpServer.Extensions;
using Rnwood.SmtpServer.Extensions.Auth;
using System.Security.Cryptography.X509Certificates;

namespace Rnwood.SmtpServer
{
    public interface IConnection
    {
        IEnumerable<IExtensionProcessor> ExtensionProcessors { get; }
        VerbMap VerbMap { get; }
        MailVerb MailVerb { get; }
        ISession Session { get; }
        Message CurrentMessage { get; }
        Encoding ReaderEncoding { get; }
        bool Authenticated { get; }
        void SetReaderEncoding(Encoding encoding);
        void SetReaderEncodingToDefault();
        void CloseConnection();
        void ApplyStreamFilter(Func<Stream, Stream> filter);
        void WriteResponse(SmtpResponse response);
        string ReadLine();
        void MessageStart(string from);
        void MessageRecipientAdd(string recipient);
        void MessageComplete();
        void CommitMessage();
        void AbortMessage();
        AuthenticationResult ValidateAuthenticationCredentials(IAuthenticationRequest authenticationRequest);
        string GetDomainName();
        bool IsAuthMechanismEnabled(IAuthMechanism mechanism);
        X509Certificate GetSSLCertificate();
        long? GetMaximumMessageSize();
    }
}