﻿using System.Windows;
using Rnwood.SmtpServer;

namespace Rnwood.Smtp4dev.MessageInspector
{
    /// <summary>
    /// Interaction logic for EmailInspectorWindow.xaml
    /// </summary>
    public partial class InspectorWindow : Window
    {
        public InspectorWindow(Message message)
        {
            InitializeComponent();
            Message = new MessageViewModel(message);
        }

        private MessageViewModel Message
        {
            get { return DataContext as MessageViewModel; }

            set
            {
                DataContext = value;
                treeView.DataContext = new[] {Message};
            }
        }

        private MessageViewModel SelectedPart
        {
            get { return treeView.SelectedItem as MessageViewModel; }
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (treeView.SelectedItem != null)
            {
                partDetailsGrid.IsEnabled = true;
            }
            else
            {
                partDetailsGrid.IsEnabled = false;
            }
        }

        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            SelectedPart.View();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SelectedPart.Save();
        }
    }
}