﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rnwood.SmtpServer.Extensions.Auth
{
    public class AuthMechanismProcessorResult
    {
        public AuthMechanismProcessorResult(AuthMechanismProcessorStatus status, SmtpResponse response = null)
        {
            Status = status;
            Response = response;
        }

        public AuthMechanismProcessorStatus Status { get; private set; }

        public SmtpResponse Response { get; private set; }
    }
}
