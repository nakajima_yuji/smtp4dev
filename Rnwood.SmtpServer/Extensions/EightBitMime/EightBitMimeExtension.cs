﻿namespace Rnwood.SmtpServer.Extensions.EightBitMime
{
    public class EightBitMimeExtension : IExtension
    {
        private class EightBitMimeExtensionProcessor : IExtensionProcessor
        {
            public EightBitMimeExtensionProcessor(IConnection connection)
            {
                var verb = new EightBitMimeDataVerb();
                connection.VerbMap.SetVerbProcessor("DATA", verb);
                connection.MailVerb.FromSubVerb.ParameterProcessorMap.SetProcessor("BODY", verb);
            }

            public string[] EHLOKeywords
            {
                get { return new[] {"8BITMIME"}; }
            }
        }

        public IExtensionProcessor CreateExtensionProcessor(IConnection connection)
        {
            return new EightBitMimeExtensionProcessor(connection);
        }
    }
}