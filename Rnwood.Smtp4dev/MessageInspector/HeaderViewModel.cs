﻿namespace Rnwood.Smtp4dev.MessageInspector
{
    internal class HeaderViewModel
    {
        public HeaderViewModel(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }

        public string Key { get; private set; }
        public string Value { get; private set; }
    }
}
