﻿namespace Rnwood.SmtpServer.Extensions.Auth.Plain
{
    internal class PlainMechanismProcessor : AuthMechanismProcessorBase
    {
        private enum States
        {
            Initial,
            AwaitingResponse
        }

        private States state;

        public PlainMechanismProcessor(IConnection connection) : base(connection)
        {
            this.state = States.Initial;
        }

        public override AuthMechanismProcessorResult ProcessRequest(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                if (this.state == States.AwaitingResponse)
                {
                    throw new SmtpServerException(new SmtpResponse(StandardSmtpResponseCode.AuthenticationFailure, "Missing auth data"));
                }

                this.state = States.AwaitingResponse;
                return new AuthMechanismProcessorResult(AuthMechanismProcessorStatus.Continue, new SmtpResponse(StandardSmtpResponseCode.AuthenticationContinue, ""));
            }

            var decodedData = DecodeBase64(data);
            var decodedDataParts = decodedData.Split('\0');
            if (decodedDataParts.Length != 3)
            {
                throw new SmtpServerException(new SmtpResponse(StandardSmtpResponseCode.AuthenticationFailure, "Auth data in incorrect format"));
            }

            var username = decodedDataParts[1];
            var password = decodedDataParts[2];
            return ValidateAuthenticationCredentials(new PlainAuthenticationRequest(username, password));
        }
    }
}
