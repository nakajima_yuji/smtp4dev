﻿using System.Collections.Generic;
using Rnwood.SmtpServer.Extensions.Auth.Anonymous;
using Rnwood.SmtpServer.Extensions.Auth.CramMd5;
using Rnwood.SmtpServer.Extensions.Auth.Login;
using Rnwood.SmtpServer.Extensions.Auth.Plain;

namespace Rnwood.SmtpServer.Extensions.Auth
{
    internal class AuthMechanismMap
    {
        private readonly Dictionary<string, IAuthMechanism> map;

        public AuthMechanismMap()
        {
            this.map = new Dictionary<string, IAuthMechanism>();
            Add(new CramMd5Mechanism());
            Add(new PlainMechanism());
            Add(new LoginMechanism());
            Add(new AnonymousMechanism());
        }

        public IAuthMechanism Get(string identifier)
        {
            IAuthMechanism result;
            this.map.TryGetValue(identifier, out result);

            return result;
        }

        public IEnumerable<IAuthMechanism> GetAll()
        {
            return this.map.Values;
        }

        private void Add(IAuthMechanism mechanism)
        {
            this.map[mechanism.Identifier] = mechanism;
        }
    }
}