﻿namespace Rnwood.SmtpServer.ConnectionVerbs.RcptSubVerbs
{
    internal class RcptSubVerbMap : VerbMap
    {
        public RcptSubVerbMap()
        {
            SetVerbProcessor("TO", new RcptToVerb());
        }
    }
}
