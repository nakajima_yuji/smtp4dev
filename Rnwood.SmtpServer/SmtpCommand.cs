﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Rnwood.SmtpServer
{
    public class SmtpCommand
    {
        private static Regex COMMANDREGEX = new Regex("(?'verb'[^ :]+)[ :]*(?'arguments'.*)");

        private readonly string verb;
        private readonly string argumentsText;
        private readonly IList<string> arguments;
        private readonly bool isValid;

        public SmtpCommand(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                this.isValid = false;
                return;
            }

            Match match = COMMANDREGEX.Match(text);
            if (!match.Success)
            {
                this.isValid = false;
                return;
            }

            this.verb = match.Groups["verb"].Value;
            this.argumentsText = match.Groups["arguments"].Value ?? "";
            this.arguments = ParseArguments(this.argumentsText);
            this.isValid = true;
        }

        internal string Verb { get { return this.verb; } }

        internal string ArgumentsText { get { return this.argumentsText; } }

        internal IList<string> Arguments { get { return this.arguments; } }

        internal bool IsValid { get { return this.isValid; } }

        private IList<string> ParseArguments(string argumentsText)
        {
            var arguments = new List<string>();

            var ltCount = 0;
            var currentArgument = new StringBuilder();

            foreach (char character in argumentsText)
            {
                switch (character)
                {
                    case '<':
                        ltCount++;
                        goto default;
                    case '>':
                        ltCount--;
                        goto default;
                    case ' ':
                    case ':':
                        if (ltCount == 0)
                        {
                            arguments.Add(currentArgument.ToString());
                            currentArgument = new StringBuilder();
                        }
                        else
                        {
                            goto default;
                        }
                        break;
                    default:
                        currentArgument.Append(character);
                        break;
                }
            }

            if (currentArgument.Length != 0)
            {
                arguments.Add(currentArgument.ToString());
            }

            return arguments;
        }
    }
}