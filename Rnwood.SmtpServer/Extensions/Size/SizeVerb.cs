﻿using System;
using System.Net.Mail;
using Rnwood.SmtpServer.ConnectionVerbs.MailSubVerbs;

namespace Rnwood.SmtpServer.Extensions.Size
{
    internal class SizeVerb : IParameterProcessor
    {
        private readonly IConnection connection;

        public SizeVerb(IConnection connection)
        {
            this.connection = connection;
        }

        public void SetParameter(string key, string value)
        {
            if (key.Equals("SIZE", StringComparison.InvariantCultureIgnoreCase))
            {
                int messageSize;

                if (int.TryParse(value, out messageSize) && messageSize > 0)
                {
                    long? maxMessageSize = this.connection.GetMaximumMessageSize();

                    if (maxMessageSize.HasValue && messageSize > maxMessageSize)
                    {
                        throw new SmtpServerException(
                            new SmtpResponse(StandardSmtpResponseCode.ExceededStorageAllocation,
                                             "Message exceeds fixes size limit"));
                    }
                }
                else
                {
                    throw new SmtpException(SmtpStatusCode.SyntaxError, "Bad message size specified");
                }
            }
        }
    }
}
