﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace Rnwood.SmtpServer
{
    [Serializable]
    public class SmtpResponse : ISerializable
    {
        protected SmtpResponse(SerializationInfo info, StreamingContext context)
        {
            if (info == null) throw new ArgumentNullException(nameof(info));

            Code = info.GetInt32(nameof(Code));
            Message = info.GetString(nameof(Message));
        }

        public SmtpResponse(StandardSmtpResponseCode code, string message, params object[] args)
            : this((int)code, message, args)
        {
        }

        private SmtpResponse(int code, string message, params object[] args)
        {
            Code = code;
            Message = string.Format(message, args);
        }

        public int Code { get; private set; }

        public string Message { get; private set; }

        private bool IsError
        {
            get { return Code >= 500 && Code <= 599; }
        }

        private bool IsSuccess
        {
            get { return Code >= 200 && Code <= 299; }
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null) throw new ArgumentNullException(nameof(info));

            info.AddValue(nameof(Code), Code);
            info.AddValue(nameof(Message), Message);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the response.
        /// </summary>
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            string[] lines = Message.Split(new string[]{"\r\n"}, System.StringSplitOptions.None);

            for (int l = 0; l < lines.Length; l++)
            {
                string line = lines[l];

                if (l == lines.Length - 1)
                {
                    result.AppendLine(Code + " " + line);
                }
                else
                {
                    result.AppendLine(Code + "-" + line);
                }
            }

            return result.ToString();
        }
    }
}