﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Rnwood.SmtpServer;

namespace Rnwood.Smtp4dev
{
    internal sealed class SessionViewModel
    {
        private readonly ISession session;

        public SessionViewModel(ISession session)
        {
            this.session = session;
        }

        public IEnumerable<Message> Messages
        {
            get
            {
                return this.session.Messages;
            }
        }


        public void ViewLog()
        {
            TempFileCollection tempFiles = new TempFileCollection();
            FileInfo msgFile = new FileInfo(tempFiles.AddExtension("txt"));
            File.WriteAllText(msgFile.FullName, session.Log, Encoding.ASCII);
            Process.Start(msgFile.FullName);
        }
    }
}