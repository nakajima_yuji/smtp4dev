﻿namespace Rnwood.SmtpServer.ConnectionVerbs
{
    internal class ConnectionVerbMap : VerbMap
    {
        public ConnectionVerbMap()
        {
            SetVerbProcessor("HELO", new HeloVerb());
            SetVerbProcessor("EHLO", new EhloVerb());
            SetVerbProcessor("QUIT", new QuitVerb());
            SetVerbProcessor("MAIL", new MailVerb());
            SetVerbProcessor("RCPT", new RcptVerb());
            SetVerbProcessor("DATA", new DataVerb());
            SetVerbProcessor("RSET", new RsetVerb());
            SetVerbProcessor("NOOP", new NoopVerb());
        }
    }
}
