﻿namespace Rnwood.SmtpServer.Extensions.Auth.Anonymous
{
    internal class AnonymousMechanismProcessor : AuthMechanismProcessorBase
    {
        public AnonymousMechanismProcessor(IConnection connection) : base(connection)
        {

        }

        public override AuthMechanismProcessorResult ProcessRequest(string data)
        {
            return ValidateAuthenticationCredentials(new AnonymousAuthenticationRequest());
        }
    }
}
