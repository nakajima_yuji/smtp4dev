﻿namespace Rnwood.SmtpServer
{
    public enum Ports
    {
        AssignAutomatically = 0,
        SMTP = 25,
        SMTPOverSSL = 465
    }
}
