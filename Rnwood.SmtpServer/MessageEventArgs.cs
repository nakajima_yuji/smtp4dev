﻿using System;

namespace Rnwood.SmtpServer
{
    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs(Message message)
        {
            Message = message;
        }

        public Message Message { get; private set; }
    }
}
