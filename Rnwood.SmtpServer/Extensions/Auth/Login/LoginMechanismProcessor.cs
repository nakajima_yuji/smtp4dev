﻿using System;
using System.Text;

namespace Rnwood.SmtpServer.Extensions.Auth.Login
{
    internal class LoginMechanismProcessor : AuthMechanismProcessorBase
    {
        private enum States
        {
            Initial,
            WaitingForUsername,
            WaitingForPassword,
            Completed
        }

        private States state;
        private string username;

        public LoginMechanismProcessor(IConnection connection) : base(connection)
        {
            this.state = States.Initial;
        }

        public override AuthMechanismProcessorResult ProcessRequest(string data)
        {
            switch (this.state)
            {
                case States.Initial:
                    this.state = States.WaitingForUsername;
                    return new AuthMechanismProcessorResult(AuthMechanismProcessorStatus.Continue, new SmtpResponse(StandardSmtpResponseCode.AuthenticationContinue, EncodeBase64("Username:")));

                case States.WaitingForUsername:
                    this.username = DecodeBase64(data);
                    this.state = States.WaitingForPassword;
                    return new AuthMechanismProcessorResult(AuthMechanismProcessorStatus.Continue, new SmtpResponse(StandardSmtpResponseCode.AuthenticationContinue, EncodeBase64("Password:")));

                case States.WaitingForPassword:
                    var password = DecodeBase64(data);
                    this.state = States.Completed;
                    return ValidateAuthenticationCredentials(new LoginAuthenticationRequest(this.username, password));

                default:
                    throw new NotImplementedException();
            }
        }
    }
}
