using System.IO;
using MimeKit;
using Rnwood.SmtpServer;

namespace Rnwood.Smtp4dev
{
    internal sealed class MessageViewModel
    {
        public MessageViewModel(Message message)
        {
            this.Message = message;
        }

        public Message Message { get; private set; }

        public string From
        {
            get { return this.Message.From; }
        }

        public string To
        {
            get { return string.Join(", ", this.Message.To); }
        }

        public string Subject
        {
            get { return MimeMessage.Load(this.Message.GetData()).Subject; }
        }

        public Message Parts
        {
            get { return this.Message; }
        }

        public bool HasBeenViewed { get; private set; }

        public void SaveToFile(FileInfo file)
        {
            HasBeenViewed = true;

            byte[] data = new byte[64 * 1024];
            int bytesRead;

            using (Stream dataStream = Message.GetData(false))
            {
                using (FileStream fileStream = file.OpenWrite())
                {
                    while ((bytesRead = dataStream.Read(data, 0, data.Length)) > 0)
                    {
                        fileStream.Write(data, 0, bytesRead);
                    }
                }
            }
        }

        public void MarkAsViewed()
        {
            HasBeenViewed = true;
        }
    }
}