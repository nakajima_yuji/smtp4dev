﻿using Rnwood.SmtpServer.ConnectionVerbs.RcptSubVerbs;

namespace Rnwood.SmtpServer.ConnectionVerbs
{
    internal class RcptVerb : IVerb
    {
        private readonly RcptSubVerbMap subVerbMap;

        public RcptVerb()
        {
            this.subVerbMap = new RcptSubVerbMap();
        }

        public void Process(IConnection connection, SmtpCommand command)
        {
            var subrequest = new SmtpCommand(command.ArgumentsText);
            var verbProcessor = this.subVerbMap.GetVerbProcessor(subrequest.Verb);
            if (verbProcessor == null)
            {
                connection.WriteResponse(new SmtpResponse(StandardSmtpResponseCode.CommandParameterNotImplemented, $"Subcommand {subrequest.Verb} not implemented"));
            }

            verbProcessor.Process(connection, subrequest);
        }
    }
}