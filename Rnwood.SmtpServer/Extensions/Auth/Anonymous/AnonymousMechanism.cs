﻿namespace Rnwood.SmtpServer.Extensions.Auth.Anonymous
{
    internal class AnonymousMechanism : IAuthMechanism
    {
        public string Identifier
        {
            get { return "ANONYMOUS"; }
        }

        public bool IsPlainText
        {
            get { return false; }
        }

        public IAuthMechanismProcessor CreateAuthMechanismProcessor(IConnection connection)
        {
            return new AnonymousMechanismProcessor(connection);
        }
    }
}