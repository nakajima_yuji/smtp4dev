﻿using Rnwood.SmtpServer.ConnectionVerbs.MailSubVerbs;

namespace Rnwood.SmtpServer.ConnectionVerbs
{
    public class MailVerb : IVerb
    {
        private readonly MailSubVerbMap subVerbMap;

        public MailVerb()
        {
            this.subVerbMap = new MailSubVerbMap();
        }

        public MailFromVerb FromSubVerb
        {
            get { return (MailFromVerb)this.subVerbMap.GetVerbProcessor("FROM"); }
        }

        public void Process(IConnection connection, SmtpCommand command)
        {
            var subrequest = new SmtpCommand(command.ArgumentsText);
            var verbProcessor = this.subVerbMap.GetVerbProcessor(subrequest.Verb);
            if (verbProcessor == null)
            {
                connection.WriteResponse(new SmtpResponse(StandardSmtpResponseCode.CommandParameterNotImplemented, $"Subcommand {subrequest.Verb} not implemented"));
            }

            verbProcessor.Process(connection, subrequest);
        }
    }
}