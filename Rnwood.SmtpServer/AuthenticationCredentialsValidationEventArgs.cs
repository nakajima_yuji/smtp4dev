﻿using System;
using Rnwood.SmtpServer.Extensions.Auth;

namespace Rnwood.SmtpServer
{
    public class AuthenticationCredentialsValidationEventArgs : EventArgs
    {
        public AuthenticationCredentialsValidationEventArgs(IAuthenticationRequest credentials)
        {
            Credentials = credentials;
        }

        private IAuthenticationRequest Credentials { get; set; }

        private AuthenticationResult AuthenticationResult { get; set; }
    }
}
