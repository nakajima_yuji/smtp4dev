﻿using System;
using System.Text;

namespace Rnwood.SmtpServer.Extensions.Auth
{
    internal abstract class AuthMechanismProcessorBase : IAuthMechanismProcessor
    {
        private readonly IConnection connection;

        public AuthMechanismProcessorBase(IConnection connection)
        {
            this.connection = connection;
        }

        public abstract AuthMechanismProcessorResult ProcessRequest(string data);

        protected string EncodeBase64(string src)
        {
            return Convert.ToBase64String(Encoding.ASCII.GetBytes(src));
        }

        protected string DecodeBase64(string data)
        {
            try
            {
                return Encoding.ASCII.GetString(Convert.FromBase64String(data ?? ""));
            }
            catch (FormatException)
            {
                throw new SmtpServerException(new SmtpResponse(StandardSmtpResponseCode.AuthenticationFailure, "Bad Base64 data"));
            }
        }

        protected string GetDomainName()
        {
            return this.connection.GetDomainName();
        }

        protected AuthMechanismProcessorResult ValidateAuthenticationCredentials(IAuthenticationRequest request)
        {
            var result = this.connection.ValidateAuthenticationCredentials(request);
            if (result == AuthenticationResult.Success)
            {
                return new AuthMechanismProcessorResult(AuthMechanismProcessorStatus.Success);
            }

            return new AuthMechanismProcessorResult(AuthMechanismProcessorStatus.Failed);
        }
    }
}
