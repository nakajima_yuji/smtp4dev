﻿namespace Rnwood.SmtpServer.Extensions.Auth.CramMd5
{
    internal class CramMd5AuthenticationRequest : IAuthenticationRequest
    {
        private readonly string username;
        private readonly string challengeResponse;
        private readonly string challenge;

        public CramMd5AuthenticationRequest(string username, string challenge, string challengeResponse)
        {
            this.username = username;
            this.challengeResponse = challengeResponse;
            this.challenge = challenge;
        }
    }
}