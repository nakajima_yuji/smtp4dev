﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Rnwood.SmtpServer.ConnectionVerbs;
using Rnwood.SmtpServer.Extensions;
using Rnwood.SmtpServer.Extensions.Auth;

namespace Rnwood.SmtpServer
{
    internal class Connection : IConnection, IDisposable
    {
        private readonly TcpClient tcpClient;
        private readonly IServerBehaviour server;

        private Stream stream;
        private StreamReader reader;
        private StreamWriter writer;

        private bool disposedValue = false;

        public Connection(IServerBehaviour server, TcpClient tcpClient)
        {
            this.VerbMap = new ConnectionVerbMap();

            this.server = server;
            this.tcpClient = tcpClient;
            this.tcpClient.ReceiveTimeout = this.server.GetReceiveTimeout(this);
            this.Session = new Session()
            {
                ClientAddress = ((IPEndPoint)this.tcpClient.Client.RemoteEndPoint).Address,
                StartDate = DateTime.Now
            };

            this.stream = tcpClient.GetStream();
            SetReaderEncodingToDefault();
        }

        public VerbMap VerbMap { get; private set; }

        public ISession Session { get; private set; }

        public Encoding ReaderEncoding { get; private set; }

        public Message CurrentMessage { get; private set; }

        public IEnumerable<IExtensionProcessor> ExtensionProcessors
        {
            get { return this.server.GetExtensions(this).Select(e => e.CreateExtensionProcessor(this)); }
        }

        public MailVerb MailVerb
        {
            get { return (MailVerb) VerbMap.GetVerbProcessor("MAIL"); }
        }

        public bool Authenticated
        {
            get { return Session.Authenticated; }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public void Start()
        {
            try
            {
                OnSessionStarted(this.Session);

                if (this.server.IsSSLEnabled(this))
                {
                    var sslStream = new SslStream(this.stream);
                    sslStream.AuthenticateAsServer(GetSSLCertificate());

                    this.stream = sslStream;
                    SetupReaderAndWriter();

                    this.Session.SecureConnection = true;
                }

                WriteResponse(new SmtpResponse(StandardSmtpResponseCode.ServiceReady, $"{GetDomainName()} smtp4dev ready"));

                while (this.tcpClient.Client.Connected)
                {
                    var command = new SmtpCommand(ReadLine());
                    OnCommandReceived(command);

                    if (!command.IsValid)
                    {
                        continue;
                    }

                    IVerb verbProcessor = VerbMap.GetVerbProcessor(command.Verb);
                    if (verbProcessor == null)
                    {
                        WriteResponse(new SmtpResponse(StandardSmtpResponseCode.SyntaxErrorCommandUnrecognised, "Command unrecognised"));
                        continue;
                    }

                    try
                    {
                        verbProcessor.Process(this, command);
                    }
                    catch (SmtpServerException exception)
                    {
                        WriteResponse(exception.SmtpResponse);
                    }
                }
            }
            catch (IOException ioException)
            {
                this.Session.SessionError = ioException.Message;
            }

            CloseConnection();

            this.Session.EndDate = DateTime.Now;
            OnSessionCompleted(this.Session);
        }

        public void CloseConnection()
        {
            if (this.reader != null)
            {
                this.reader.Close();
                this.reader = null;
            }

            if (this.writer != null)
            {
                this.writer.Flush();
                this.writer.Close();
                this.writer = null;
            }

            if (this.stream != null)
            {
                this.stream.Close();
                this.stream = null;
            }

            if (this.tcpClient.Connected)
            {
                this.tcpClient.Close();
            }
        }

        public void ApplyStreamFilter(Func<Stream, Stream> filter)
        {
            this.stream = filter(this.stream);
            SetupReaderAndWriter();
        }

        public void SetReaderEncodingToDefault()
        {
            SetReaderEncoding(new ASCIISevenBitTruncatingEncoding());
        }

        public void SetReaderEncoding(Encoding encoding)
        {
            this.ReaderEncoding = encoding;
            SetupReaderAndWriter();
        }

        public void WriteResponse(SmtpResponse response)
        {
            WriteLine(response.ToString().TrimEnd());
        }

        public void WriteLine(string text, params object[] arg)
        {
            var formattedText = string.Format(text, arg);
            this.Session.AppendToLog(formattedText);
            this.writer.WriteLine(formattedText);
        }

        public string ReadLine()
        {
            var text = this.reader.ReadLine();
            if (text == null) throw new IOException("Client disconnected");

            this.Session.AppendToLog(text);
            return text;
        }

        public void MessageStart(string from)
        {
            OnMessageStarting(from);
            this.CurrentMessage = new Message(this.Session);
            this.CurrentMessage.From = from;
        }

        public void MessageRecipientAdd(string recipient)
        {
            OnMessageRecipientAdding(this.CurrentMessage, recipient);
            this.CurrentMessage.ToList.Add(recipient);
        }

        public void MessageComplete()
        {
            OnMessageCompleted();
        }

        public void CommitMessage()
        {
            Message message = this.CurrentMessage;
            this.Session.Messages.Add(message);
            this.CurrentMessage = null;
            OnMessageReceived(message);
        }

        public void AbortMessage()
        {
            this.CurrentMessage = null;
        }

        public string GetDomainName()
        {
            return this.server.DomainName;
        }

        public AuthenticationResult ValidateAuthenticationCredentials(IAuthenticationRequest authenticationRequest)
        {
            var result = this.server.ValidateAuthenticationCredentials(this, authenticationRequest);
            if (result == AuthenticationResult.Success)
            {
                this.Session.SetAuthenticationCredentials(authenticationRequest);
            }

            return result;
        }

        public bool IsAuthMechanismEnabled(IAuthMechanism mechanism)
        {
            return this.server.IsAuthMechanismEnabled(this, mechanism);
        }

        public X509Certificate GetSSLCertificate()
        {
            return this.server.GetSSLCertificate(this);
        }

        public long? GetMaximumMessageSize()
        {
            return this.server.GetMaximumMessageSize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposedValue) return;

            if (disposing)
            {
                CloseConnection();
            }

            this.disposedValue = true;
        }

        protected virtual void OnSessionStarted(ISession session)
        {
            this.server.OnSessionStarted(this, session);
        }

        protected virtual void OnSessionCompleted(ISession session)
        {
            this.server.OnSessionCompleted(this, session);
        }

        protected virtual void OnCommandReceived(SmtpCommand smtpCommand)
        {
            this.server.OnCommandReceived(this, smtpCommand);
        }

        protected virtual void OnMessageStarting(string from)
        {
            this.server.OnMessageStarting(this, from);
        }

        protected virtual void OnMessageReceived(Message message)
        {
            this.server.OnMessageReceived(this, message);
        }

        protected virtual void OnMessageRecipientAdding(Message message, string recipient)
        {
            this.server.OnMessageRecipientAdding(this, message, recipient);
        }

        protected virtual void OnMessageCompleted()
        {
            this.server.OnMessageCompleted(this);
        }

        private void SetupReaderAndWriter()
        {
            this.writer = new StreamWriter(this.stream, this.ReaderEncoding) { AutoFlush = true, NewLine = "\r\n" };
            this.reader = new StreamReader(this.stream, this.ReaderEncoding);
        }
    }
}