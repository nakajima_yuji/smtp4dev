﻿using System;
using System.Collections.Generic;

namespace Rnwood.SmtpServer.ConnectionVerbs.MailSubVerbs
{
    public class ParameterProcessorMap
    {
        private readonly Dictionary<string, IParameterProcessor> _processors =
            new Dictionary<string, IParameterProcessor>(StringComparer.InvariantCultureIgnoreCase);

        public void SetProcessor(string key, IParameterProcessor connection)
        {
            _processors[key] = connection;
        }

        private IParameterProcessor GetProcessor(string key)
        {
            IParameterProcessor result = null;
            _processors.TryGetValue(key, out result);
            return result;
        }

        public void Process(string[] tokens, bool throwOnUnknownParameter)
        {
            Process(new ParameterParser(tokens), throwOnUnknownParameter);
        }

        private void Process(string parametersString, bool throwOnUnknownParameter)
        {
            Process(new ParameterParser(parametersString), throwOnUnknownParameter);
        }

        private void Process(ParameterParser parameters, bool throwOnUnknownParameter)
        {
            foreach (Parameter parameter in parameters.Parameters)
            {
                IParameterProcessor parameterProcessor = GetProcessor(parameter.Name);

                if (parameterProcessor != null)
                {
                    parameterProcessor.SetParameter(parameter.Name, parameter.Value);
                }
                else if (throwOnUnknownParameter)
                {
                    throw new SmtpServerException(
                        new SmtpResponse(StandardSmtpResponseCode.SyntaxErrorInCommandArguments,
                                         "Parameter {0} is not recognised", parameter.Name));
                }
            }
        }
    }
}