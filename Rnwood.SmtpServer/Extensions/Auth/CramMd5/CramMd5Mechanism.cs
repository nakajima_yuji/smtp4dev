﻿namespace Rnwood.SmtpServer.Extensions.Auth.CramMd5
{
    internal class CramMd5Mechanism : IAuthMechanism
    {
        public string Identifier
        {
            get { return "CRAM-MD5"; }
        }

        public bool IsPlainText
        {
            get { return false; }
        }

        public IAuthMechanismProcessor CreateAuthMechanismProcessor(IConnection connection)
        {
            return new CramMd5MechanismProcessor(connection);
        }
    }
}