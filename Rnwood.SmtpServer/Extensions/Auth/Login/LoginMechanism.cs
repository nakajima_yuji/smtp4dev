﻿namespace Rnwood.SmtpServer.Extensions.Auth.Login
{
    internal class LoginMechanism : IAuthMechanism
    {
        public string Identifier
        {
            get { return "LOGIN"; }
        }

        public bool IsPlainText
        {
            get { return true; }
        }

        public IAuthMechanismProcessor CreateAuthMechanismProcessor(IConnection connection)
        {
            return new LoginMechanismProcessor(connection);
        }
    }
}