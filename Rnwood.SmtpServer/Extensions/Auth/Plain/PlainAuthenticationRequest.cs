﻿namespace Rnwood.SmtpServer.Extensions.Auth.Plain
{
    internal class PlainAuthenticationRequest : IAuthenticationRequest
    {
        private readonly string username;
        private readonly string password;

        public PlainAuthenticationRequest(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }
}