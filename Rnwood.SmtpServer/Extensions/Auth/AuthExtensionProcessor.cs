﻿using System.Collections.Generic;
using System.Linq;

namespace Rnwood.SmtpServer.Extensions.Auth
{
    internal class AuthExtensionProcessor : IExtensionProcessor
    {
        private readonly IConnection connection;

        public AuthExtensionProcessor(IConnection connection)
        {
            this.connection = connection;
            MechanismMap = new AuthMechanismMap();
            connection.VerbMap.SetVerbProcessor("AUTH", new AuthVerb(this));
        }

        public AuthMechanismMap MechanismMap { get; private set; }

        public string[] EHLOKeywords
        {
            get
            {
                IEnumerable<IAuthMechanism> mechanisms = MechanismMap.GetAll();

                if (mechanisms.Any())
                {
                    return new[]
                               {
                                   "AUTH=" + string.Join(" ", mechanisms.Where(IsMechanismEnabled).Select(m => m.Identifier).ToArray()),
                                   "AUTH " + string.Join(" ", mechanisms.Select(m => m.Identifier).ToArray())
                               };
                }
                else
                {
                    return new string[0];
                }
            }
        }

        public bool IsMechanismEnabled(IAuthMechanism mechanism)
        {
            return this.connection.IsAuthMechanismEnabled(mechanism);
        }
    }
}
