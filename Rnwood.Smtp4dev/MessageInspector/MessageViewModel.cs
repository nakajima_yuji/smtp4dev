﻿using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using anmar.SharpMimeTools;
using Microsoft.Win32;
using MimeKit;
using Rnwood.SmtpServer;

namespace Rnwood.Smtp4dev.MessageInspector
{
    internal sealed class MessageViewModel : INotifyPropertyChanged
    {
        private readonly SharpMimeMessage message;
        private readonly MimeMessage mimeMessage;
        private bool isSelected;

        public MessageViewModel(Message message)
        {
            this.message = new SharpMimeMessage(message.GetData());
            this.mimeMessage = MimeMessage.Load(message.GetData());
        }

        public MessageViewModel(SharpMimeMessage message)
        {
            this.message = message;
        }

        public bool IsSelected
        {
            get { return this.isSelected; }

            set
            {
                this.isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        public MessageViewModel[] Children
        {
            get { return this.message.Cast<SharpMimeMessage>().Select(part => new MessageViewModel(part)).ToArray(); }
        }

        public HeaderViewModel[] Headers
        {
            get { return this.mimeMessage.Headers.Select(de => new HeaderViewModel((string)de.Field, (string)de.Value)).ToArray(); }
        }

        public string Data
        {
            get { return this.mimeMessage.ToString(); }
        }

        public string Body
        {
            get { return this.mimeMessage.TextBody; }
        }

        public string Type
        {
            get { return this.mimeMessage.Body.ContentType.MediaType + "/" + this.mimeMessage.Body.ContentType.MediaSubtype; }
        }

        public long Size
        {
            get { return this.mimeMessage.ToString().LongCount(); }
        }

        public string Disposition
        {
            get { return this.message.Header.ContentDisposition; }
        }

        public string Encoding
        {
            get { return this.message.Header.ContentTransferEncoding; }
        }

        public string Name
        {
            get { return this.mimeMessage.Body.ContentType.Name ?? "Unnamed" + ": " + this.MimeType + " (" + this.Size + " bytes)"; }
        }

        public string Subject
        {
            get { return this.mimeMessage.Subject; }
        }

        private string MimeType
        {
            get { return this.mimeMessage.Body.ContentType.MimeType; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Save()
        {
            SaveFileDialog dialog = new SaveFileDialog();

            string filename = (this.mimeMessage.Body.ContentType.Name ?? "Unnamed");

            if (string.IsNullOrEmpty(Path.GetExtension(this.mimeMessage.Body.ContentType.Name)))
            {
                filename = filename + (MIMEDatabase.GetExtension(MimeType));
            }

            dialog.FileName = filename;
            dialog.Filter = "File (*.*)|*.*";

            if (dialog.ShowDialog() == true)
            {
                using (FileStream stream = File.OpenWrite(dialog.FileName))
                {
                    this.mimeMessage.WriteTo(stream);
                }
            }
        }

        public void View()
        {
            string extn = Path.GetExtension(this.mimeMessage.Body.ContentType.Name ?? "Unnamed");

            if (string.IsNullOrEmpty(extn))
            {
                extn = MIMEDatabase.GetExtension(MimeType) ?? ".part";
            }

            TempFileCollection tempFiles = new TempFileCollection();
            FileInfo msgFile = new FileInfo(tempFiles.AddExtension(extn.TrimStart('.')));

            using (FileStream stream = msgFile.OpenWrite())
            {
                this.mimeMessage.WriteTo(stream);
            }

            Process.Start(msgFile.FullName);
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}