﻿namespace Rnwood.SmtpServer
{
    public interface IVerb
    {
        void Process(IConnection connection, SmtpCommand command);
    }
}
