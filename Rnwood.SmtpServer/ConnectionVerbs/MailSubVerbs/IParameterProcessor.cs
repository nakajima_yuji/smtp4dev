﻿namespace Rnwood.SmtpServer.ConnectionVerbs.MailSubVerbs
{
    public interface IParameterProcessor
    {
        void SetParameter(string key, string value);
    }
}