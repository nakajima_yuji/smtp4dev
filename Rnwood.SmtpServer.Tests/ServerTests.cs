﻿using System.Net.Sockets;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Rnwood.SmtpServer.Tests
{
    [TestClass]
    public class ServerTests
    {
        [TestMethod]
        public void Run_Blocks()
        {
            var server = new DefaultServer(Ports.AssignAutomatically);
            var shouldBeRunning = true;
            var runThread = new Thread(() =>
                                              {
                                                  server.Run();

                                                  //If we get here before Stop) is called then fail
                                                  Assert.IsFalse(shouldBeRunning);
                                              });
            runThread.Start();

            while (!server.IsRunning)
            {
                //spin!
            }
            Assert.IsTrue(runThread.IsAlive);
            shouldBeRunning = false;
            server.Stop();
            runThread.Join();
        }

        [TestMethod]
        public void Start_IsRunning()
        {
            var server = StartServerWithWait();
            Assert.IsTrue(server.IsRunning);
            server.Stop();
        }

        [TestMethod]
        [ExpectedException(typeof (SocketException))]
        public void StartOnInusePort_StartupExceptionThrown()
        {
            var server1 = StartServerWithWait();

            try
            {
                var server2 = new DefaultServer(server1.PortNumber);
                server2.Start();
            }
            finally
            {
                server1.Stop();
            }
        }

        [TestMethod]
        public void Stop_NotRunning()
        {
            var server = StartServerWithWait();
            server.Stop();
            Assert.IsFalse(server.IsRunning);
        }

        [TestMethod]
        public void Start_CanConnect()
        {
            var server = StartServerWithWait();

            try
            {
                var client = new TcpClient();
                client.Connect("localhost", server.PortNumber);
                client.Close();
            }
            finally
            {
                server.Stop();
            }
        }

        private Server StartServerWithWait()
        {
            var server = new DefaultServer(Ports.AssignAutomatically);
            server.Start();
            for (int i = 0; i < 10; i++)
            {
                if (server.IsRunning)
                {
                    break;
                }

                Thread.Sleep(1000);
            }

            return server;
        }
    }
}