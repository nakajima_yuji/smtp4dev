﻿namespace Rnwood.SmtpServer.Extensions.Auth.Plain
{
    internal class PlainMechanism : IAuthMechanism
    {
        public string Identifier
        {
            get { return "PLAIN"; }
        }

        public bool IsPlainText
        {
            get { return true; }
        }

        public IAuthMechanismProcessor CreateAuthMechanismProcessor(IConnection connection)
        {
            return new PlainMechanismProcessor(connection);
        }
    }
}