﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rnwood.SmtpServer.ConnectionVerbs
{
    public class VerbMap
    {
        private readonly Dictionary<string, IVerb> processorVerbs = new Dictionary<string, IVerb>();

        public void SetVerbProcessor(string verb, IVerb verbProcessor)
        {
            this.processorVerbs[verb] = verbProcessor;
        }

        public IVerb GetVerbProcessor(string verb)
        {
            return this.processorVerbs
                .Where(x => string.Equals(x.Key, verb, StringComparison.InvariantCultureIgnoreCase))
                .Select(y => y.Value)
                .FirstOrDefault();
        }
    }
}