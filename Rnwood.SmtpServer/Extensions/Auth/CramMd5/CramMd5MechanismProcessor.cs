﻿using System;
using System.Text;

namespace Rnwood.SmtpServer.Extensions.Auth.CramMd5
{
    internal class CramMd5MechanismProcessor : AuthMechanismProcessorBase
    {
        private enum States
        {
            Initial,
            AwaitingResponse
        }

        private readonly Random random;
        private string challenge;

        public CramMd5MechanismProcessor(IConnection connection) : base(connection)
        {
            this.random = new Random();
        }

        public override AuthMechanismProcessorResult ProcessRequest(string data)
        {
            if (this.challenge == null)
            {
                this.challenge = CreateCallenge();
                var base64Challenge = EncodeBase64(this.challenge);
                return new AuthMechanismProcessorResult(AuthMechanismProcessorStatus.Continue, new SmtpResponse(StandardSmtpResponseCode.AuthenticationContinue, base64Challenge));
            }

            var response = DecodeBase64(data);
            var responseparts = response.Split(' ');
            if (responseparts.Length != 2)
            {
                throw new SmtpServerException(new SmtpResponse(StandardSmtpResponseCode.AuthenticationFailure, "Response in incorrect format - should be USERNAME RESPONSE"));
            }

            var username = responseparts[0];
            var hash = responseparts[1];
            return ValidateAuthenticationCredentials(new CramMd5AuthenticationRequest(username, this.challenge, hash));
        }

        private string CreateCallenge()
        {
            StringBuilder challenge = new StringBuilder();
            challenge.Append(this.random.Next(Int16.MaxValue));
            challenge.Append(".");
            challenge.Append(DateTime.Now.Ticks.ToString());
            challenge.Append("@");
            challenge.Append(GetDomainName());
            return challenge.ToString();
        }
    }
}
