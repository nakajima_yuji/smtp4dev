﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Rnwood.SmtpServer
{
    [Serializable]
    public class SmtpServerException : Exception
    {
        protected SmtpServerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            if (info == null) throw new ArgumentNullException(nameof(info));

            SmtpResponse = info.GetValue(nameof(SmtpResponse), SmtpResponse.GetType()) as SmtpResponse;
        }

        public SmtpServerException(SmtpResponse smtpResponse) : this(smtpResponse, null)
        {
        }

        private SmtpServerException(SmtpResponse smtpResponse, Exception innerException)
            : base(smtpResponse.Message, innerException)
        {
            SmtpResponse = smtpResponse;
        }

        public SmtpResponse SmtpResponse { get; private set; }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null) throw new ArgumentNullException(nameof(info));

            base.GetObjectData(info, context);
            info.AddValue(nameof(SmtpResponse), SmtpResponse, SmtpResponse.GetType());
        }
    }
}