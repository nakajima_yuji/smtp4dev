﻿namespace Rnwood.SmtpServer.Extensions.StartTls
{
    public class StartTlsExtension : IExtension
    {
        private class StartTlsExtensionProcessor : IExtensionProcessor
        {
            public StartTlsExtensionProcessor(IConnection connection)
            {
                Connection = connection;
                Connection.VerbMap.SetVerbProcessor("STARTTLS", new StartTlsVerb());
            }

            private IConnection Connection { get; set; }

            public string[] EHLOKeywords
            {
                get
                {
                    if (!Connection.Session.SecureConnection)
                    {
                        return new[] { "STARTTLS" };
                    }

                    return new string[] { };
                }
            }
        }

        public IExtensionProcessor CreateExtensionProcessor(IConnection connection)
        {
            return new StartTlsExtensionProcessor(connection);
        }
    }
}