using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Rnwood.SmtpServer.Extensions.Auth;

namespace Rnwood.SmtpServer
{
    internal class Session : ISession
    {
        private readonly StringBuilder log = new StringBuilder();
        private IAuthenticationRequest authenticationCredentials;

        public Session()
        {
            this.authenticationCredentials = null;
            Messages = new List<Message>();
        }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public IPAddress ClientAddress { get; set; }

        public string ClientName { get; set; }

        public bool SecureConnection { get; set; }

        public List<Message> Messages { get; private set; }

        public bool CompletedNormally { get; set; }

        public string SessionError { get; set; }

        public bool Authenticated
        {
            get { return this.authenticationCredentials != null; }
        }

        public string Log
        {
            get { return this.log.ToString(); }
        }

        public void AppendToLog(string text)
        {
            this.log.AppendLine(text);
        }

        public void SetAuthenticationCredentials(IAuthenticationRequest request)
        {
            this.authenticationCredentials = request;
        }
    }
}